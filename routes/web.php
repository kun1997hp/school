<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'IndexController@index']);
Route::prefix('dashboard')->group(function () {

    Route::get('/test', 'UserController@create');

    Route::prefix('users')->group(function () {
        Route::get('/', 'UserController@index')->name('users.index');
        Route::get('create', 'UserController@create')->name('users.create');
        Route::post('/', ['as' => 'users.store', 'uses' => 'UserController@store']);
        Route::get('/{id}/edit', 'UserController@edit')->name('users.edit');
        Route::put('/{id}', 'UserController@update')->name('users.update');
        Route::delete('/{id}/delete', 'UserController@destroy')->name('users.destroy');
    });

    Route::prefix('employees')->group(function () {
        Route::get('/', 'EmployeeController@index')->name('employees.index');
//        Route::get('/{id}', ['as' => 'employees.show', 'uses' => 'EmployeeController@show']);
        Route::get('/create', ['as' => 'employees.create', 'uses' => 'EmployeeController@create']);
        Route::post('/', ['as' => 'employees.store', 'uses' => 'EmployeeController@store']);
        Route::get('/{id}/edit', ['as' => 'employees.edit', 'uses' => 'EmployeeController@edit']);
        Route::put('/{id}', ['as' => 'employees.update', 'uses' => 'EmployeeController@update']);
        Route::delete('/{id}', 'EmployeeController@destroy')->name('employees.destroy');
    });

    Route::prefix('teachers')->group(function () {
        Route::get('/', ['as' => 'teachers.index', 'uses' => 'TeacherController@index']);
    //    Route::get('/teachers/{id}', ['as' => 'teachers.show', 'uses' => 'TeacherController@show']);
        Route::get('/create', ['as' => 'teachers.create', 'uses' => 'TeacherController@create']);
        Route::post('/', ['as' => 'teachers.store', 'uses' => 'TeacherController@store']);
        Route::get('/{id}/edit', ['as' => 'teachers.edit', 'uses' => 'TeacherController@edit']);
        Route::post('/teachers/{id}', ['as' => 'teachers.update', 'uses' => 'TeacherController@update']);
        Route::delete('/{id}', ['as' => 'teachers.destroy', 'uses' => 'TeacherController@destroy']);
    });

    Route::prefix('students')->group(function () {
        Route::get('/', ['as' => 'students.index', 'uses' => 'StudentController@index']);
//        Route::get('/{id}', ['as' => 'students.show', 'uses' => 'StudentController@show']);
        Route::get('/create', ['as' => 'students.create', 'uses' => 'StudentController@create']);
        Route::post('/', ['as' => 'students.store', 'uses' => 'StudentController@store']);
        Route::get('/{id}/edit', ['as' => 'students.edit', 'uses' => 'StudentController@edit']);
        Route::post('/{id}', ['as' => 'students.update', 'uses' => 'StudentController@update']);
        Route::delete('/{id}', ['as' => 'students.destroy', 'uses' => 'StudentController@destroy']);
    });

    Route::get('/classrooms', ['as' => 'classrooms.index', 'uses' => 'ClassroomController@index']);
//    Route::get('/classrooms/{id}', ['as' => 'classrooms.show', 'uses' => 'ClassroomController@show']);
    Route::get('/classrooms/create', ['as' => 'classrooms.create', 'uses' => 'ClassroomController@create']);
    Route::post('/classrooms', ['as' => 'classrooms.store', 'uses' => 'ClassroomController@store']);
    Route::get('/classrooms/{id}/edit', ['as' => 'classrooms.edit', 'uses' => 'ClassroomController@edit']);
    Route::post('/classrooms/{id}', ['as' => 'classrooms.update', 'uses' => 'ClassroomController@update']);
    Route::delete('/classrooms/{id}', ['as' => 'classrooms.destroy', 'uses' => 'ClassroomController@destroy']);

    Route::get('/subjects', ['as' => 'subjects.index', 'uses' => 'SubjectController@index']);
//    Route::get('/subjects/{id}', ['as' => 'subjects.show', 'uses' => 'SubjectController@show']);
    Route::get('/subjects/create', ['as' => 'subjects.create', 'uses' => 'SubjectController@create']);
    Route::post('/subjects', ['as' => 'subjects.store', 'uses' => 'SubjectController@store']);
    Route::get('/subjects/{id}/edit', ['as' => 'subjects.edit', 'uses' => 'SubjectController@edit']);
    Route::post('/subjects/{id}', ['as' => 'subjects.update', 'uses' => 'SubjectController@update']);
    Route::delete('/subjects/{id}', ['as' => 'subjects.destroy', 'uses' => 'SubjectController@destroy']);

    Route::get('/shifts', ['as' => 'shifts.index', 'uses' => 'ShiftController@index']);
//    Route::get('/shifts/{id}', ['as' => 'shifts.show', 'uses' => 'ShiftController@show']);
//    Route::get('/shifts/create', ['as' => 'shifts.create', 'uses' => 'ShiftController@create']);
//    Route::post('/shifts', ['as' => 'shifts.store', 'uses' => 'ShiftController@store']);
//    Route::get('/shifts/{id}/edit', ['as' => 'shifts.edit', 'uses' => 'ShiftController@edit']);
//    Route::post('/shifts/{id}', ['as' => 'shifts.update', 'uses' => 'ShiftController@update']);
//    Route::delete('/shifts/{id}', ['as' => 'shifts.destroy', 'uses' => 'ShiftController@destroy ']);

    Route::prefix('classes')->group(function () {
        Route::get('/', ['as' => 'classes.index', 'uses' => 'ClassController@index']);
    //    Route::get('/{id}', ['as' => 'classes.show', 'uses' => 'ClassController@show']);
        Route::get('/create', ['as' => 'classes.create', 'uses' => 'ClassController@create']);
        Route::post('/', ['as' => 'classes.store', 'uses' => 'ClassController@store']);
        Route::prefix('{id}')->group(function () {
            Route::get('/edit', ['as' => 'classes.edit', 'uses' => 'ClassController@edit']);
            Route::post('/', ['as' => 'classes.update', 'uses' => 'ClassController@update']);
            Route::delete('/', ['as' => 'classes.destroy', 'uses' => 'ClassController@destroy']);

            Route::get('/add-classroom-view', [
                'as' => 'classes.add.classroom.view',
                'uses' => 'ClassController@addClassroomView'
            ]);

            Route::post('/add-classroom', [
            'as' => 'classes.add.classroom',
            'uses' => 'ClassController@addClassroom'
            ]);
            Route::prefix('student-list')->group(function () {
                Route::get('/', [
                    'as' => 'classes.student.list',
                    'uses' => 'ClassController@studentListView'
                ]);
                Route::post('/add-student-in-view', [
                    'as' => 'classes.save.many.students',
                    'uses' => 'ClassController@saveManyStudents'
                ]);
                Route::get('/add-normal', [
                    'as' => 'classes.add.normal.student.view',
                    'uses' => 'ClassController@addNormalStudentView'
                ]);
                Route::post('/add-normal', [
                    'as' => 'classes.add.normal.student',
                    'uses' => 'ClassController@addNormalStudent'
                ]);
                Route::get('/add', [
                    'as' => 'classes.add.student.view',
                    'uses' => 'ClassController@addStudentView'
                ]);
                Route::post('/{student_id}', [
                    'as' => 'classes.add.student',
                    'uses' => 'ClassController@addStudent'
                ]);
                Route::get('/add-teacher', [
                    'as' => 'classes.add.teacher.view',
                    'uses' => 'ClassController@addTeacherView'
                ]);
                Route::post('/add-teacher', [
                    'as' => 'classes.add.teacher',
                    'uses' => 'ClassController@addTeacher'
                ]);
                Route::delete('/{student_id}/delete', [
                    'as' => 'classes.delete.student',
                    'uses' => 'ClassController@deleteStudent'
                ]);
            });
        });
    });
});
