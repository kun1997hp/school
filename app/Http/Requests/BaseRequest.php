<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;


class BaseRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $errors = array_flatten((new ValidationException($validator))->errors());
        throw new HttpResponseException(response()->json(
            [
                'code' => 422,
                'message' => $errors[0],
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

}
