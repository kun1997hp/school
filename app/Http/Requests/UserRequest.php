<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'role' => [
                'required',
                Rule::in([1,2,3])
            ],
            'status' => [
                'required',
                Rule::in([0,1])
            ]


        ];
    }

    public function attributes()
    {
        return [
            'role' => 'Quyền hạn',
            'txtEmail' => 'Email',
            'txtPass' => 'Password',
            'status' => 'Trạng thái'
        ];
    }

    public function messages()
    {
        return [
            'required' => '::attribute không được để trống.'
        ];
    }
}
