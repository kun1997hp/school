<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\EmployeeRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::get()->toArray();
        return view('employee.list', compact('employees'));
    }

    public function create()
    {
        return view('employee.create');
    }

    public function store(EmployeeRequest $request)
    {
        $emp = Employee::create([
            'name' => $request->name,
            'email' => $request->email,
            'birth' => $request->birth,
            'phone' => $request->phone,
            'address' => $request->address,
            'image' =>$request->image
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make("123456"),
            'role' => 2,
            'status' => 0,
            'person_id' => $emp->id
        ]);

        return redirect()->route('employees.index')->with('success', 'Tạo mới thành công');
    }

    public function edit($id)
    {
        $employee = Employee::where('id', $id)->first();
        return view('employee.edit', compact('employee'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect(route('employees.edit'))
                ->withErrors($validator)
                ->withInput();
        }
        Employee::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'birth' => $request->birth,
            'phone' => $request->phone,
            'address' => $request->address,
            'image' => $request->image
        ]);
         User::where('person_id', $id)->update([
             'name' => $request->name,
             'email' =>$request->email
         ]);

         return redirect(route('employees.index'))->with('success', 'Cập nhật thành công');

    }

    public function destroy($id)
    {
        $employee = Employee::find($id);
        if (!$employee) {
            return redirect()->back();
        }
        $user = User::where('person_id', $id)->first();
        $employee->delete();
        if ($user) {
            $user->delete();
        }
        return redirect(route('employees.index'))->with('success', 'Xóa thành công');
    }

}
