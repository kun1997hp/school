<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeacherRequest;
use App\Subject;
use App\Teacher;
use App\TeacherSub;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TeacherController extends Controller
{
    public function index()
    {
        $teachers = Teacher::get()->toArray();
        foreach ($teachers as &$teacher) {
            $teacher_id = $teacher['id'];
            $subjects = TeacherSub::where('teacher_id', $teacher_id)->get(['subject_id']);
            $teachSubIds = [];
            foreach ($subjects as $subject) {
                $subTitle = Subject::where('id', $subject['subject_id'])->first(['name']);
                array_push($teachSubIds, $subTitle['name']);
            }
            $teacher['subjects'] = $teachSubIds;
        }
        return view('teacher.list', compact('teachers'));
    }

    public function create()
    {
        $subjects = Subject::get();
        return view('teacher.create', compact('subjects'));
    }

    public function store(TeacherRequest $request)
    {
        $subjects = Subject::get(['id']);

        $teacher = Teacher::create([
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'birth' => $request->birth,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'status' => $request->status
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make("123456"),
            'person_id' => $teacher->id,
            'role' => 3,
            'status' => 0
        ]);

        foreach ($subjects as $subject) {
            $sub_name = "subject".$subject->id;
            if ($request->$sub_name) {
                TeacherSub::create([
                    "teacher_id" => $teacher->id,
                    "subject_id" => $request->$sub_name
                ]);
            }
        }

        return redirect(route('teachers.index'))->with('success', 'Tạo mới thành công');
    }

    public function edit($id)
    {
        $teacher = Teacher::where('id', $id)->first();
        $subjects = Subject::get();
        $teacher_subs = TeacherSub::where('teacher_id', $id)->get();

        foreach ($subjects as &$subject) {
            foreach ($teacher_subs as $teacher_sub) {
                if ($teacher_sub->subject_id == $subject->id) {
                    $subject['check'] = true;
                }
            }
        }

        return view('teacher.edit')->with(compact('teacher'))->with(compact('subjects'));
    }

    public function update(Request $request, $id)
    {
        $teacher = Teacher::where('id', $id);
        $user = User::where('person_id', $id);
        $subjects = Subject::get();
        $teacher_subs = TeacherSub::where('teacher_id', $id)->get();

        if ($teacher) {
            $teacher->update([
               'name' => $request->name,
                'birth' => $request->birth,
                'phone' => $request->phone,
                'address' => $request->address,
                'gender' => $request->gender,
                'status' =>$request->status,
            ]);
            if ($user) {
                $user->update([
                   'name' => $request->name
                ]);
            }
            foreach ($teacher_subs as $teacher_sub) {
                $teacher_sub->delete();
            }

            foreach ($subjects as $subject) {
                $sub_name = "subject".$subject->id;
                if ($request->$sub_name) {
                    TeacherSub::create([
                        "teacher_id" => $id,
                        "subject_id" => $request->$sub_name
                    ]);
                }
            }

            return redirect()->route('teachers.index')->with('success', 'Cập nhật thành công');
        }
        return redirect()->route('teachers.index')->with('warning', 'Giảng viên không tồn tại');
    }

    public function destroy($id)
    {
        $teacher = Teacher::where('id', $id);

        if (!$teacher){
            return redirect()->back()->with('warning', 'Giảng viên không tồn tại');
        }

        $teacher->delete();
        $user = User::where('person_id', $id);
        if (!$user){
            return redirect(route('teachers.index'));
        }
        $user->delete();
        return redirect(route('teachers.index'))->with('success', 'Xóa thành công');
    }
}
