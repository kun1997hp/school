<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::get();
        return view('subject.list', compact('subjects'));
    }

    public function create()
    {
        return view('subject.create');
    }

    public function store(Request $request)
    {
        Subject::create([
            'name' => $request->name,
            'status' => $request->status
        ]);
        return redirect()->route('subjects.index')->with('success', 'Tạo mới thành công');
    }

    public function edit($id)
    {
        $subject = Subject::where('id', $id)->first();
        if ($subject) {
            return view('subject.edit', compact('subject'));
        }
        return redirect()->back()->with('warning', 'Môn học không tồn tại');
    }

    public function update(Request $request, $id)
    {
        $subject = Subject::where('id', $id)->first();
        $subject->update([
            'name' => $request->name,
            'status' => $request->status
        ]);
        return redirect()->route('subjects.index')->with('success', 'Cập nhật thành công');
    }

    public function destroy($id)
    {
        $subject = Subject::where('id', $id)->first();
        if ($subject) {
            $subject->delete();
            return redirect()->route('subjects.index')->with('success', 'Xóa thành công');
        }
        return redirect()->back()->with('warning', 'Môn học không tồn tại');
    }
}
