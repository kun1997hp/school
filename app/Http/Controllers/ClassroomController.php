<?php

namespace App\Http\Controllers;

use App\Classroom;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    public function index()
    {
        $classrooms = Classroom::get();
        return view('classroom.list', compact('classrooms'));
    }

    public function create()
    {
        return view('classroom.create');
    }

    public function store(Request $request)
    {
        Classroom::create([
            'name' => $request->name,
            'address' => $request->address,
            'capacity' => $request->capacity,
            'status' => $request->status
        ]);
        return redirect()->route('classrooms.index')->with('success', 'Tạo mới thành công');
    }

    public function edit($id)
    {
        $classroom = Classroom::where('id', $id)->first();

        if ($classroom)
        {
            return view('classroom.edit', compact('classroom'));
        }

        return redirect()->back()->with('warning', 'Phòng học không tồn tại, vui lòng kiểm tra lại.');
    }

    public function update(Request $request, $id)
    {
        $classroom = Classroom::where('id', $id)->first();
        $classroom->update([
            'name' => $request->name,
            'address' => $request->address,
            'capacity' => $request->capacity,
            'status' => $request->status
        ]);
        return redirect()->route('classrooms.index')->with('success', "Cập nhật thành công");
    }

    public function destroy($id)
    {
        $classroom = Classroom::where('id', $id)->first();
        if ($classroom) {
            $classroom->delete();
            return redirect()->back();
        }
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
