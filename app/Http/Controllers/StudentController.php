<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::get();
        return view('student.list', compact('students'));
    }

    public function create()
    {
        return view('student.create');
    }

    public function store(StudentRequest $request)
    {
        Student::create([
            'name' => $request->name,
            'birth' => $request->birth,
            'address' => $request->address,
            'phone' => $request->phone,
            'email' => $request->email,
            'gender' => $request->gender,
            'status' => $request->status
        ]);
        return redirect()->route('students.index')->with('success', 'Tạo mới thành công');
    }

    public function edit($id)
    {
        $student = Student::where('id', $id)->first();
        if ($student) {
            return view('student.edit', compact('student'));
        }
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $student = Student::where('id', $id);
        if ($student) {
            $student->update([
                'name' => $request->name,
                'birth' => $request->birth,
                'address' => $request->address,
                'phone' => $request->phone,
                'gender' => $request->gender,
                'status' => $request->status
            ]);
            return redirect()->route('students.index')->with('success', 'Cập nhật thành công');
        }
        return redirect()->route('students.index');
    }

    public function destroy($id)
    {
        $student = Student::where('id', $id);
        if ($student) {
            $student->delete();
            return redirect()->route('students.index')->with('success', 'Xóa thành công');
        }
        return redirect()->route('students.index')->with('warning', 'Học viên không tồn tại');
    }
}
