<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function test()
    {
        return view('user.create');
    }
    public function index()
    {
        $users = User::get()->toArray();
        $active = 'active';
        return view('user.list', compact('users'));
    }

    public function show($id)
    {
        //
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'status'=>$request->status,
            'password' => Hash::make($request->password)
        ]);

        return redirect(route('users.index'))->with('success', 'Tạo mới thành công');
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();

        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('users.edit'))
                ->withErrors($validator)
                ->withInput();
        }

        $user_pass = User::where('id', $id)->first('password');
        $pass = null;
        if (!$request->password) {
            $pass = Hash::make($request->password);
        } else {
            $pass = $user_pass;
        }

        User::where('id', $id)
            ->update([
                'email' => $request->email,
                'name' => $request->name,
                'role' => $request->role,
                'status' => $request->status,
                'password' => $user_pass
            ]);
        return redirect(route('users.index'))->with('success', 'Cập nhật thành công');

    }

    public function destroy($id)
    {
        $user = User::where('id', $id)->first();

        if (!$user) {
            return redirect(route('users.index'))->with('warning', 'Người dùng không tồn tại!');
        }
         $user->delete();
        return redirect(route('users.index'))->with('success', 'Xóa thành công');
    }
}
