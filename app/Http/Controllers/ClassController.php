<?php

namespace App\Http\Controllers;

use App\ClassClassroomModel;
use App\ClassModel;
use App\Classroom;
use App\ClassStudent;
use App\Student;
use App\Subject;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    public function index()
    {
        $classes = ClassModel::get();
        foreach ($classes as &$class) {
            $students = ClassStudent::where('class_id', $class['id'])->get();
            $count_std = count($students);
            $class['count_std'] = $count_std;

            $classroom = ClassClassroomModel::where('class_id', $class->id)->first();
            $class['classroom'] = $classroom['name'];

            $subject = Subject::where('id', $class->subject_id)->first('name');
            $class['sub_name'] = $subject['name'];
        }
        return view('class.list', compact('classes'));
    }

    public function show()
    {
        //
    }

    public function create()
    {
        $subjects = Subject::get();
        return view('class.create', compact('subjects'));
    }

    public function store(Request $request)
    {
        $employee_id = 1;
        ClassModel::create([
            'name' => $request->name,
            'max' => $request->max,
            'employee_id' => $employee_id,
            'subject_id' => $request->subject
        ]);
        return redirect()->route('classes.index')->with('success', 'Thêm mới thành công');
    }

    public function edit($id)
    {
        $class = ClassModel::where('id', $id)->get();
        if ($class) {
            return view('class.edit', compact('class'));
        }
        return redirect()->back()->with('danger', 'Lớp học không tồn tại');
    }

    public function update(Request $request, $id)
    {
        $class = ClassModel::where('id', $id);
        if ($class) {
            $class->update([
                'name' => $request->name,
                'max' => $request->max,
                'subject_id' => $request->subject
            ]);
            return redirect()->route('classes.index')->with('success', 'Cập nhật thành công');
        }
        return redirect()->back()->with('danger', 'Lớp học không tồn tại');
    }

    public function destroy($id)
    {
        $class = ClassModel::where('id', $id);
        if ($class) {
            $class->delete();
            return redirect()->route('classes.index')->with('success', 'Xóa thành công');
        }
        return redirect()->back()->with('danger', 'Lớp học không tồn tại');
    }

    public function addClassroomView($id)
    {
        $class = ClassModel::where('id', $id)->first(['name']);
        $classrooms = Classroom::get();
        return view('class.createclassroom', compact('id'))
            ->with(compact('class'))
            ->with(compact('classrooms'));
    }

    public function addClassroom($id)
    {
//        $class_classroom = ClassClassroomModel::where('class_id', $id)->first()
    }

    public function studentListView($id)
    {
        $class = ClassModel::where('id', $id)->first(['name']);
        $class_students = ClassStudent::where('class_id', $id)->get();
        $all_students = Student::get();
        $students = [];
        foreach ($class_students as $class_student) {
            $student = Student::where('id', $class_student->student_id)->first();
            array_push($students, $student);
        }
        return view('class.student_list', compact('class_students'))
            ->with(compact('class'))
            ->with(compact('students'))
            ->with(compact('all_students'))
            ->with(compact('id'));
    }

    public function addStudentView($id)
    {
        $class = ClassModel::where('id', $id)->first(['name']);
        $students = Student::get();
        $class_students = ClassStudent::where('class_id', $id)->get();

        foreach ($students as &$student) {
            foreach ($class_students as $class_student) {
                if ($class_student->student_id === $student->id) {
                    $student['in_class'] = true;
                }
            }
        }

        return view('class.add_student')->with(compact('class'))
            ->with(compact('id'))
            ->with(compact('students'));
    }

    public function addStudent($id, $student_id)
    {
        ClassStudent::create([
            'class_id' => $id,
            'student_id' => $student_id
        ]);
        return redirect()->route('classes.add.student.view', $id);
    }

//    public function addNormalStudentView($id)
//    {
////        return view('')
//        dd("eeee");
//    }

    public function saveManyStudents(Request $request)
    {
        dd("hehe");
    }
}
