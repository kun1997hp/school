<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeacherSub extends Model
{
    use SoftDeletes;

    protected $table = 'teachersubs';

    protected $fillable = [
        'subject_id', 'teacher_id'
    ];

    public $timestamps = true;
}
