<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassStudent extends Model
{
    use SoftDeletes;

    protected $table = 'class_students';

    protected $fillable = [
        'class_id', 'student_id'
    ];

    public $timestamps = true;
}
