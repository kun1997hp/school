<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassClassroomModel extends Model
{
    use SoftDeletes;

    protected $table = 'class_classrooms';

    protected $fillable = ['class_id', 'classroom_id'];

    public $timestamps = true;
}
