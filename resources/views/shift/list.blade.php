@extends('master')
@section('title', 'Danh sách người dùng')
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Quản lý User</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Danh sách người dùng</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <a class="btn btn-primary float-right" href="{!! route('users.create') !!}">Thêm mới</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Họ tên</th>
                                <th>Email</th>
                                <th>Quyền</th>
                                <th>Trạng thái</th>
                                <th>Ngày tạo</th>
                                <th>Ngày cập nhật</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user["id"]}}</td>
                                <td>{{$user["name"]}}</td>
                                <td>{{$user["email"]}}</td>
                                @if($user["role"] == 1)
                                    <td>Quản trị viên</td>
                                @endif
                                @if($user["role"] == 2)
                                    <td>Nhân viên</td>
                                @endif
                                @if($user["role"] == 3)
                                    <td>Giảng viên</td>
                                @endif
                                @if($user["status"] == 1)
                                    <td class="text-center" "><i class="fas fa-eye"></i></td>
                                @endif
                                @if($user["status"] == 0)
                                    <td class="text-center"><i class="fas fa-eye-slash"></i></td>
                                @endif
                                <td>{{$user["created_at"]}}</td>
                                <td>{{$user["updated_at"]}}</td>
                                <td><a class="text-center nav-link" href="users/{{$user["id"]}}/edit"><i class="fas fa-edit"></i></a></td>
                                <td>
                                    <form action="{{route('users.destroy', $user["id"])}}" method="post">
                                        {{ method_field('delete') }}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button onclick="return confirm('Are you sure you want to Remove?');" class="btn btn-default text-center" type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

