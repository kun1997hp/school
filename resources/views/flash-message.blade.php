@if ($message = Session::get('success'))
    <div class="message-alert alert alert-success alert-block" id="mess">
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="message-alert alert alert-danger alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('warning'))
    <div class="message-alert alert alert-warning alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('info'))
    <div class="message-alert alert alert-info alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
