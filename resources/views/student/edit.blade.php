@extends('master')
@section('title', 'Cập nhật học viên')
@section('student')
    <a href="{{route('students.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-user-graduate"></i>
        <p>Danh sách học viên</p>
    </a>
@overwrite
@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cập nhật thông tin học viên</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Cập nhật học viên</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('main-content')
    <section class="content">
        <div class="container-fluid col-9">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Cập nhật học viên</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal needs-validation" method="POST" action="{{route('students.update', $student['id'])}}" novalidate>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Họ tên</label>
                            <div class="col-sm-6">
                                <input name="name" class="form-control" id="inputEmail3" placeholder="Họ tên"
                                       value="{{ old('name', isset($student)? $student['name']:null)}}" required>
                                <div class="invalid-feedback">
                                    Trường họ tên không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Ngày sinh</label>
                            <div class="col-sm-6">
                                <input class="form-control" placeholder="Ngày sinh" name="birth"
                                       value="{{ old('birth', isset($student)? $student['birth']:null)}}" required>
                                <div class="invalid-feedback">
                                    Trường ngày sinh không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Số điện thoại</label>
                            <div class="col-sm-6">
                                <input class="form-control" placeholder="Số điện thoại" name="phone"
                                       value="{{ old('phone', isset($student)? $student['phone']:null)}}" required>
                                <div class="invalid-feedback">
                                    Trường số điện thoại không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Địa chỉ</label>
                            <div class="col-sm-6">
                                <input class="form-control" placeholder="Địa chỉ" name="address"
                                       value="{{ old('address', isset($student)? $student['address']:null)}}" required>
                                <div class="invalid-feedback">
                                    Trường địa chỉ không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Giới tính</label>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        @if($student['gender'] == 1)
                                            <input class="form-check-input" type="radio" name="gender" value="1" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="gender" value="1">
                                        @endif
                                        <label class="form-check-label">Nam</label>
                                    </div>
                                    <div class="form-check">
                                        @if($student['gender'] == 2)
                                            <input class="form-check-input" type="radio" name="gender" value="2" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="gender" value="2">
                                        @endif
                                        <label class="form-check-label">Nữ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        @if($student['status'] == 1)
                                            <input class="form-check-input" type="radio" name="status" value="1" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="status" value="1">
                                        @endif
                                        <label class="form-check-label">Hoạt động</label>
                                    </div>
                                    <div class="form-check">
                                        @if($student['status'] == 0)
                                            <input class="form-check-input" type="radio" name="status" value="0" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="status" value="0">
                                        @endif
                                        <label class="form-check-label">Không hoạt động</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Cập nhật</button>
                        <a class="btn btn-danger float-right" href="{{route('students.index')}}">Hủy</a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </section>
@endsection

