@extends('master')
@section('title', 'Danh sách giảng viên')
@section('teacher')
    <a href="{{route('teachers.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-user-tie"></i>
        <p>Danh sách giảng viên</p>
    </a>
@overwrite
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Quản lý giảng viên</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Danh sách giảng viên</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <div class="float-left flash-mss">@include('flash-message')</div>
                        <a class="btn btn-primary float-right" href="{{route('teachers.create')}}">Thêm mới</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Họ tên</th>
                                <th>Email</th>
                                <th>Ngày sinh</th>
                                <th>Điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Giới tính</th>
                                <th>Trạng thái</th>
                                <th>Môn giảng dạy</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($teachers as $teacher)
                                <?php $i++ ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$teacher["name"]}}</td>
                                <td>{{$teacher["email"]}}</td>
                                <td>{{$teacher["birth"]}}</td>
                                <td>{{$teacher["phone"]}}</td>
                                <td>{{$teacher["address"]}}</td>
                                @if($teacher["gender"] == 1)
                                    <td>Nam</td>
                                @elseif($teacher["gender"] == 2)
                                    <td>Nữ</td>
                                @endif
                                @if($teacher["status"] == 1)
                                    <td class="text-center"><i class="fas fa-eye"></i></td>
                                @endif
                                @if($teacher["status"] == 0)
                                    <td class="text-center"><i class="fas fa-eye-slash"></i></td>
                                @endif
                                <td>
                                    @foreach ($teacher['subjects'] as $subject)
                                        {{ $subject }},
                                        @endforeach
                                </td>
                                <td><a class="text-center nav-link" href="teachers/{{$teacher["id"]}}/edit"><i class="fas fa-edit"></i></a></td>
                                <td>
                                    <form action="{{route('teachers.destroy', $teacher["id"])}}" method="post">
                                        {{ method_field('delete') }}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button onclick="return confirm('Are you sure you want to Remove?');" class="btn btn-default text-center" type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

