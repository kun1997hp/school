<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('web/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{url('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{url('web/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('web/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('web/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ url('web/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{url('web/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{url('web/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('web/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{url('web/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{url('web/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{url('web/plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{url('web/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link">
            <img src="{{url('web/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Website Quản Lý</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{url('web/dist/img/avt2.png')}}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">User Name</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" id="left-menu" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        @section('dash')
                        <a href="{{route('dashboard')}}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item user-nav">
                        @section('user')
                        <a href="{{url('dashboard/users')}}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>Danh sách người dùng</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('employee')
                        <a href="{{route('employees.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>Danh sách nhân viên</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('teacher')
                        <a href="{{route('teachers.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-user-tie"></i>
                            <p>Danh sách giảng viên</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('student')
                        <a href="{{route('students.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-user-graduate"></i>
                            <p>Danh sách học viên</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('classroom')
                        <a href="{{route('classrooms.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-building"></i>
                            <p>Danh sách phòng học</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('subject')
                        <a href="{{route('subjects.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>Danh sách môn học</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('shift')
                        <a href="{{route('shifts.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-clock"></i>
                            <p>Danh sách ca học</p>
                        </a>
                        @show
                    </li>
                    <li class="nav-item">
                        @section('class')
                        <a href="{{route('classes.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-book-reader"></i>
                            <p>Danh sách lớp học</p>
                        </a>
                        @show
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @yield('content-header')
        <!-- /.content-header -->

        <!-- Main content -->
        @yield('main-content')
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2019.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{url('web/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('web/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{url('web/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Select2 -->
<script src="{{ url('web/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- DataTables -->
<script src="{{url('web/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{url('web/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<!-- ChartJS -->
<script src="{{url('web/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{url('web/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{url('web/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{url('web/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('web/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{url('web/plugins/moment/moment.min.js')}}"></script>
<script src="{{url('web/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{url('web/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{url('web/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{url('web/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('web/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{url('web/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('web/dist/js/demo.js')}}"></script>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
<script>
    $('div.flash-mss').fadeOut(3000);
</script>
<script>
    $(document).ready(function() {
        $("ul:eq(1) > li").click(function() {
            $('ul:eq(1) > li > a').removeClass("active");
            $(this).addClass("active");
        });
    })
</script>
@yield('script')
</body>
</html>
