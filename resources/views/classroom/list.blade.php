@extends('master')
@section('title', 'Danh sách phòng học')
@section('classroom')
    <a href="{{route('classrooms.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-building"></i>
        <p>Danh sách phòng học</p>
    </a>
@overwrite
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Quản lý Phòng học</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Danh sách phòng học</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <div class="float-left flash-mss">@include('flash-message')</div>
                        <a class="btn btn-primary float-right" href="{!! route('classrooms.create') !!}">Thêm mới</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên phòng học</th>
                                <th>Địa chỉ</th>
                                <th>Sức chứa tối đa</th>
                                <th>Trạng thái</th>
                                <th>Ngày tạo</th>
                                <th>Ngày cập nhật</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt = 0 ?>
                            @foreach($classrooms as $classroom)
                                <?php $stt++ ?>
                            <tr>
                                <td>{{$stt}}</td>
                                <td>{{$classroom["name"]}}</td>
                                <td>{{$classroom["address"]}}</td>
                                <td>{{$classroom["capacity"]}}</td>
                                @if($classroom["status"] == 1)
                                    <td class="text-center"><i class="fas fa-eye"></i></td>
                                @endif
                                @if($classroom["status"] == 0)
                                    <td class="text-center"><i class="fas fa-eye-slash"></i></td>
                                @endif
                                <td>{{$classroom["created_at"]}}</td>
                                <td>{{$classroom["updated_at"]}}</td>
                                <td><a class="text-center nav-link" href="classrooms/{{$classroom["id"]}}/edit"><i class="fas fa-edit"></i></a></td>
                                <td>
                                    <form action="{{route('classrooms.destroy', $classroom["id"])}}" method="post">
                                        {{ method_field('delete') }}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button onclick="return confirm('Are you sure you want to Remove?');" class="btn btn-default text-center" type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $e(('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            }));
        });
    </script>
@endsection

