@extends('master')
@section('title', 'Cập nhật phòng học')
@section('classroom')
    <a href="{{route('classrooms.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-building"></i>
        <p>Danh sách phòng học</p>
    </a>
@overwrite
@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cập nhật thông tin phòng học</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Cập nhật phòng học</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('main-content')
    <section class="content">
        <div class="container-fluid col-9">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Cập nhật phòng học</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="{{route('classrooms.update', $classroom["id"])}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Tên phòng học</label>
                            <div class="col-sm-6">
                                <input name="name" class="form-control" id="inputEmail3" placeholder="Tên phòng học"
                                       value="{!! old('name', isset($classroom)? $classroom['name']:null) !!}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Địa chỉ</label>
                            <div class="col-sm-6">
                                <input name="address" class="form-control" id="inputEmail3" placeholder="Địa chỉ"
                                       value="{!! old('address', isset($classroom)? $classroom['address']:null) !!}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Sức chứa tối đa</label>
                            <div class="col-sm-6">
                                <input name="capacity" class="form-control" id="inputEmail3" placeholder="Số lượng tối đa có thể chứa (sinh viên)"
                                       value="{!! old('capacity', isset($classroom)? $classroom['capacity']:null) !!}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        @if($classroom['status'] == 1)
                                            <input class="form-check-input" type="radio" name="status" value="1" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="status" value="1">
                                        @endif
                                        <label class="form-check-label">Hoạt động</label>
                                    </div>
                                    <div class="form-check">
                                        @if($classroom['status'] == 0)
                                            <input class="form-check-input" type="radio" name="status" value="0" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="status" value="0">
                                        @endif
                                        <label class="form-check-label">Không hoạt động</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Cập nhật</button>
                        <a class="btn btn-danger float-right" href="{{route('classrooms.index')}}">Hủy</a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </section>
@endsection

