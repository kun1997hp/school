@extends('master')
@section('title', 'Tạo mới nhân viên')
@section('employee')
    <a href="{{route('employees.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-user"></i>
        <p>Danh sách nhân viên</p>
    </a>
@overwrite
@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Thêm nhân viên</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Thêm mới nhân viên</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('main-content')
    <section class="content">
        <div class="container-fluid col-9">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Thêm mới nhân viên</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal needs-validation" method="POST" action="{{route('employees.store')}}" novalidate>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Họ tên</label>
                            <div class="col-sm-6">
                                <input name="name" class="form-control" id="inputEmail3" placeholder="Họ tên" required>
                                <div class="invalid-feedback">
                                    Trường họ tên không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" required>
                                <div class="invalid-feedback">
                                    Trường email không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Ngày sinh</label>
                            <div class="col-sm-6">
                                <input class="form-control" placeholder="Ngày sinh" name="birth">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Số điện thoại</label>
                            <div class="col-sm-6">
                                <input class="form-control" placeholder="Số điện thoại" name="phone" required>
                                <div class="invalid-feedback">
                                    Trường số điện thoại không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Địa chỉ</label>
                            <div class="col-sm-6">
                                <input class="form-control" placeholder="Địa chỉ" name="address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Ảnh</label>
                            <div class="col-sm-6">
                                <input type="file" name="image">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Tạo</button>
                        <a class="btn btn-danger float-right" href="{{route('users.index')}}">Hủy</a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </section>
@endsection

