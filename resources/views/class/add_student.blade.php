@extends('master')
@section('title', 'Danh sách học viên')
@section('class')
    <a href="{{route('classes.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-building"></i>
        <p>Danh sách lớp học</p>
    </a>
@overwrite
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Thêm học viên vào lớp {{ $class['name'] }}</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('classes.student.list', ['id' => $id])}}">{{ $class['name'] }}</a></li>
                        <li class="breadcrumb-item active">Thêm học viên vào lớp</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <div class="float-left flash-mss">@include('flash-message')</div>
                        <a class="btn btn-primary float-right ml-2" href="{{route('classes.add.normal.student.view',$id)}}">Thêm thủ công</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ tên</th>
                                <th>Email</th>
                                <th>Ngày sinh</th>
                                <th>Điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Giới tính</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($students as $student)
                                <?php $i++ ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$student["name"]}}</td>
                                <td>{{$student["email"]}}</td>
                                <td>{{$student["birth"]}}</td>
                                <td>{{$student["phone"]}}</td>
                                <td>{{$student["address"]}}</td>
                                @if($student["gender"] == 1)
                                    <td>Nam</td>
                                @elseif($student["gender"] == 2)
                                    <td>Nữ</td>
                                @endif
                                @if($student["status"] == 1)
                                    <td class="text-center"><i class="fas fa-eye"></i></td>
                                @endif
                                @if($student["status"] == 0)
                                    <td class="text-center"><i class="fas fa-eye-slash"></i></td>
                                @endif
                                <td class="text-center">
                                    @if ($student["in_class"])
                                        <i class="note-icon-menu-check"></i>
                                    @else
                                        <form action="{{route('classes.add.student', ["id" => $id, "student_id" => $student["id"]])}}" method="post">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <button class="btn btn-default text-center" type="submit">Thêm</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

