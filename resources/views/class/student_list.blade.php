@extends('master')
@section('title', 'Lớp học')
@section('class')
    <a href="{{route('classes.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-user-graduate"></i>
        <p>Danh sách lớp học</p>
    </a>
@overwrite
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Chi tiết lớp {{ $class['name'] }}</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('classes.index') }}">Danh sách lớp</a></li>
                        <li class="breadcrumb-item active">Danh sách học viên lớp {{ $class['name'] }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <div class="float-left flash-mss">@include('flash-message')</div>
                        <a class="btn btn-primary float-right ml-2" href="{{route('classes.add.student.view',$id)}}">Thêm học viên</a>
                        <a class="btn btn-primary float-right" href="{{route('classes.add.teacher.view', $id)}}">Chọn giảng viên</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('classes.save.many.students', $id)}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group row">
                                <div class="col-2">
                                    <label class="col-10">Thêm học viên:</label>
                                </div>
                                <div class="col-3">
                                    <select name="dshv" class="select2 float-left" multiple="multiple" data-placeholder="Chọn học viên"
                                            style="width: 100%;">
                                        @foreach($all_students as $a_student)
                                        <option>{{ $a_student->name }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary float-left">Thêm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ tên</th>
                                <th>Email</th>
                                <th>Ngày sinh</th>
                                <th>Điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Giới tính</th>
                                <th>Trạng thái</th>
                                <th>Ghi chú</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($students as $student)
                                <?php $i++ ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$student["name"]}}</td>
                                <td>{{$student["email"]}}</td>
                                <td>{{$student["birth"]}}</td>
                                <td>{{$student["phone"]}}</td>
                                <td>{{$student["address"]}}</td>
                                @if($student["gender"] == 1)
                                    <td>Nam</td>
                                @elseif($student["gender"] == 2)
                                    <td>Nữ</td>
                                @endif
                                @if($student["status"] == 1)
                                    <td class="text-center"><i class="fas fa-eye"></i></td>
                                @endif
                                @if($student["status"] == 0)
                                    <td class="text-center"><i class="fas fa-eye-slash"></i></td>
                                @endif
                                <td>{{$student["note"]}}</td>
                                <td>
                                    <form action="{{route('classes.delete.student', ['id' => $id, 'student_id' => $student["id"]])}}" method="post">
                                        {{ method_field('delete') }}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-default text-center" type="submit"><i class="note-icon-close"></i></button>
                                    </form>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            })
        });
    </script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            })

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'MM/DD/YYYY hh:mm A'
                }
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Timepicker
            $('#timepicker').datetimepicker({
                format: 'LT'
            })

            //Bootstrap Duallistbox
            $('.duallistbox').bootstrapDualListbox()

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            $('.my-colorpicker2').on('colorpickerChange', function(event) {
                $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
            });
        })
    </script>
@endsection

