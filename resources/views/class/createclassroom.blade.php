@extends('master')
@section('title', 'Tạo mới lớp học')
@section('class')
    <a href="{{route('classes.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-building"></i>
        <p>Danh sách lớp học</p>
    </a>
@overwrite
@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Chọn phòng học</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('classes.index')}}">Danh sách lớp học</a></li>
                        <li class="breadcrumb-item active">Chọn phòng học</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('main-content')
    <section class="content">
        <div class="container-fluid col-9">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Chọn phòng học</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="{{route('classes.add.classroom', $id)}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Tên lớp học: </label>
                            <div class="col-sm-5">
                                <input name="name" class="form-control" value="{{ $class['name'] }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Chọn địa điểm học</label>
                            <div class="col-sm-5">
                                <select class="form-control" name="classroom">
                                    <option>Chọn địa điểm học</option>
                                    @foreach($classrooms as $classroom)
                                        <option name="{{ $classroom->id }}" value="{{ $classroom->id }}">{{ $classroom['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Tạo</button>
                        <a class="btn btn-danger float-right" href="{{route('classes.index')}}">Hủy</a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </section>
@endsection

