@extends('master')
@section('title', 'Cập nhật lớp học')
@section('classes')
    <a href="{{route('classes.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-building"></i>
        <p>Danh sách lớp học</p>
    </a>
@overwrite
@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cập nhật thông tin lớp học</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('classes.index')}}">Danh sách lớp học</a></li>
                        <li class="breadcrumb-item active">Cập nhật lớp học</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('main-content')
    <section class="content">
        <div class="container-fluid col-9">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Cập nhật lớp học</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="{{route('classes.update', $class["id"])}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Tên lớp học</label>
                            <div class="col-sm-6">
                                <input name="name" class="form-control" id="inputEmail3" placeholder="Tên phòng học"
                                       value="{!! old('name', isset($class)? $class['name']:null) !!}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Số học viên</label>
                            <div class="col-sm-6">
                                <input name="max" class="form-control" placeholder="Số học viên tối đa"
                                       value="{!! old('max', isset($class)? $class['max']:null) !!}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Môn học</label>
                            <div class="col-sm-6">
                                <label>Select</label>
                                <select class="form-control">
                                    <option>option 1</option>
                                    <option>option 2</option>
                                    <option>option 3</option>
                                    <option>option 4</option>
                                    <option>option 5</option>
                                </select>
                            </div>
                        </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Cập nhật</button>
                        <a class="btn btn-danger float-right" href="{{route('classes.index')}}">Hủy</a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </section>
@endsection

