@extends('master')
@section('title', 'Danh sách lớp học')
@section('class')
    <a href="{{route('classes.index')}}" class="nav-link active">
        <i class="nav-icon fas fa-building"></i>
        <p>Danh sách lớp học</p>
    </a>
@overwrite
@section('content-header')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Quản lý lớp học</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Danh sách lớp học</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body">
                        <div class="float-left flash-mss">@include('flash-message')</div>
                        <a class="btn btn-primary float-right" href="{!! route('classes.create') !!}">Thêm mới</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên lớp học</th>
                                <th>Môn học</th>
                                <th>Số học viên/Max</th>
                                <th>Thêm học viên</th>
                                <th>Phòng học</th>
                                <th>Ca học</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt = 0 ?>
                            @foreach($classes as $class)
                                <?php $stt++ ?>
                            <tr>
                                <td>{{$stt}}</td>
                                <td>{{$class["name"]}}</td>
                                <td>{{$class["sub_name"]}}</td>
                                <td>{{ $class["count_std"] }}/{{$class["max"]}}</td>
                                <td>
                                    <a href="{{ route('classes.student.list', $class["id"]) }}"><button type="button" class="btn btn-warning">Xem học viên</button></a>
                                </td>
                                <td>
                                    @if ($class["classroom"])
                                        {{ $class["classroom"] }}
                                    @else
                                        <a href="{{ route('classes.add.classroom.view', $class["id"]) }}"><button type="button" class="btn btn-primary">Cập nhật phòng học</button></a>
                                    @endif
                                </td>
                                <td>
                                    <a><button type="button" class="btn btn-success">Xem ca học</button></a>
                                </td>
                                <td><a class="text-center nav-link" href="classes/{{$class["id"]}}/edit"><i class="fas fa-edit"></i></a></td>
                                <td>
                                    <form action="{{route('classes.destroy', $class["id"])}}" method="post">
                                        {{ method_field('delete') }}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button onclick="return confirm('Are you sure you want to Remove?');" class="btn btn-default text-center" type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

