@extends('master')
@section('title', 'Cập nhật người dùng')
@section('content-header')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cập nhật thông tin user</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Cập nhật user</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
@section('user')
    <a href="{{url('dashboard/users')}}" class="nav-link active">
        <i class="nav-icon fas fa-users"></i>
        <p>Danh sách người dùng</p>
    </a>
@overwrite
@section('main-content')
    <section class="content">
        <div class="container-fluid col-9">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Cập nhật User</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal needs-validation" method="POST" action="{{route('users.update', $user["id"])}}" novalidate>
                    @method('PUT')
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Họ tên</label>
                            <div class="col-sm-6">
                                <input name="name" class="form-control" id="validationCustom01" placeholder="Họ tên"
                                       value="{!! old('name', isset($user)? $user['name']:null) !!}" required>
                                <div class="invalid-feedback">
                                    Trường họ tên không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="validationCustom02" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" id="validationCustom02" placeholder="Email" name="email"
                                       value="{!! old('email', isset($user)? $user['email']:null) !!}" required>
                                <div class="invalid-feedback">
                                    Trường email không được để trống.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Quyền hạn</label>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        @if($user['role'] == 1)
                                            <input class="form-check-input" type="radio" name="role" value="1" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="role" value="1">
                                        @endif
                                        <label class="form-check-label">Quản trị viên</label>
                                    </div>
                                    <div class="form-check">
                                        @if($user['role'] == 2)
                                            <input class="form-check-input" type="radio" name="role" value="2" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="role" value="2">
                                        @endif
                                        <label class="form-check-label">Nhân viên</label>
                                    </div>
                                    <div class="form-check">
                                        @if($user['role'] == 3)
                                            <input class="form-check-input" type="radio" name="role" value="3" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="role" value="3">
                                        @endif
                                        <label class="form-check-label">Giảng viên</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        @if($user['status'] == 1)
                                            <input class="form-check-input" type="radio" name="status" value="1" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="status" value="1">
                                        @endif
                                        <label class="form-check-label">Hoạt động</label>
                                    </div>
                                    <div class="form-check">
                                        @if($user['status'] == 0)
                                            <input class="form-check-input" type="radio" name="status" value="0" checked>
                                        @else
                                            <input class="form-check-input" type="radio" name="status" value="0">
                                        @endif
                                        <label class="form-check-label">Không hoạt động</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Cập nhật</button>
                        <a class="btn btn-danger float-right" href="{{route('users.index')}}">Hủy</a>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
    </section>
@endsection

